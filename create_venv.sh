#!/bin/bash

# This script is expecting the user to pass a parameter when executing it.
# That parameter is to represent the name of the virtual environment.
# If no parameter is passed, the name of the virtual environment will be venv by default

virtual_env_path=""

if [ -n "$1" ]; then
		echo "As specified, the virtual environment will be name '$1'"
		python -m venv "$1"
		virtual_env_path="$1/Scripts/activate"
	else
		echo "The virtual environment name was not specified and has been defaulted to 'venv'"
		python -m venv venv
		virtual_env_path="venv/Scripts/activate"
fi

echo -e
echo -e  "If the virtual environment was not activated, likely because the script was run like so: bash create_virtualenv.sh venv_name."
echo -e "Simply run: source virtual_env_name/Scripts/activate"
echo -e "Now your virtual environment should be activated and can be deactivated."
echo -e "For future reference, run the script like so: source virtual_env_name/Scripts/activate"
echo -e

source ./$virtual_env_path
