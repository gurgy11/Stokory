# Stokory - The Simple Inventory Management System

A simple inventory management system that has just begun its development.

## Goal of Stokory

The goal behind the creation of Stokory is to create a fully working web application that could actually be useful for some people's needs. I am currently in a Programmer Analyst college program but have enjoyed coding and tinkering with software for years prior. Stokory is my personal challenge to finally put my programming knowledge to use and have a good entry to my portfolio.

## Technology Behind Stokory

At the time of this writing, I have dabbled, but not nearly mastered, any specific programming language or framework. However, in this past recent year, I have found myself enjoying my time quite a bit with Python alongside the Flask framework.

I feel as though, that by using Python and Flask rather than say, Java with SpringBoot or PHP with Laravel (which I have both had my moments with), the Flask framework is incredibly lightweight and I get to create or make my own solutions. I actually find it easier to develop this way as it gives me the opportunity to be creative with my approaches.

Despite that, one area of programming that I do not nearly have enough knowledge or experience with, is testing. I may be wrong but that might be a common occurrence among many of us who are mainly self-taught.

## What Will Stokory Be Capable Of?

While I cannot guarantee anything, I am often one to go with the flow and I'm not very certain on the viability of that approach.

However, since Stokory is going to be a website/web application with the main purpose of managing an inventory of products.  Based on that fact, here are the first parts of the system to be implemented:

1. **Goods/Products**

   * This portion of Stokory is going to be the most commonly used/useful section. From here, you will be able to manage your products, add new ones, update them, remove them, sort them, those sorts of features your'd expect. Categories and subcategories can also be created and assigned to products or certain features.

2. **Expenses**

   * For features and ways to evaluate and manage your expenses. Anything that costs the user, can be reported here. Business expenses, essentially. View by date, amounts, or other filters.

3. **Documents**

   * The documents section will mainly be used for managing incoming and outgoing orders in a spreadsheet-like format. These can be downloaded, edited in other software like Microsoft Excel and also uploaded with the goal of it being very "modular" in the sense that it will work with ease and harmony with other software.

4. **Reports**

   * Sometimes data is far easier to read and understand when it is visualized, you would come here for that. Otherwise it also includes the usual types of business reports in a well visualized and formatted manner.

5. **Customers**

   * Customers/clients are a huge factor for any business and this section is where they can be managed. You can find contact details, previous orders, contracts, these sorts of things.

6. **Suppliers**
   * Similarly to customers, and if you have an inventory of products or items for your business, you have suppliers. This is where you manage and maintain them.

Other than those six main sections, there are definitely many more to add but I must start small and with the essentials.

If I try to set too many requirements at once, I have a tendency of getting way to sidetracked or doing too many things at once. By having the foundations put in place, the rest will come together like puzzle pieces.

Ultimately, Stokory should become a fully capable solution for at least small businesses. But whether or not it comes to that, it's the learning process that's important to me.

As of now, the software will be composed of HTML, CSS, and JavaScript for the frontend, like many other websites, and the backend, or where the data is fetched, set to, and manipulated from will be Python based with the help of Flask.
